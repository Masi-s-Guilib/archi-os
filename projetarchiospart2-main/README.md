Le réseau à mettre en place est un réseau NAT 10.0.2.0/24
où 10.0.2.1 est la gateway ( Par default sur VirtualBox)

La configuration des 2 machines est la suivante:

fedora : 10.0.2.10
debian : 10.0.2.30

Avant tout deploiement, il est nécessaire de se connecter en ssh aux 2 machines afin d'enregistrer leur fingerprint pour procéder au déploiement des playbooks.
Ensuite, une fois les 2 finger print enregistrés. La commande à réaliser est la suivante:

ansible-playbook predeploy-playbook.yml -u root -k

!Ce premier module nécessite le programme sshpass. (apt-get install sshpass)

Il va vous être demandé un mot de passe pour l'utilisateur de déploiement user-ansible. Celui va être déployé sur les machines.
Ce mot de passe est à retenir car il va être utilisé ultérieurement.
Ce user-ansible disposent des droits sudos.

Ce premier playbook a aussi permit de réaliser une paire de clefs ecdsa. La clef privé  générée en local a été enregistré dans le repertoire .ssh de l'utilisateur qui a lancé le playbook.
Les clefs publics ont été distribués aux 2 machines pour les utilisateurs fraichement créés "user-ansible".

Dans le fichier hosts sont definis les variables tel que les mots de passe et les utilisateurs LDAP par défaut de l'infrastructure.
Toutes les manipulations sur la machine fedora se faisant en sudo, le rôle de déploiement de la Samba-ad sur fedora se fera en root.

Certaines taches sur debian necessitent aussi l'elevation de privilège.
Il sera alors nécessaire de saisir le mot de passe créé ci-avant lors de l'execution de la commande suivante:

ansible-playbook playbook-full.yml -K

Ce playbook réunie le déploiement des roles sur la machine fedora et debian.