<?php
$CONFIG = array (
  'htaccess.RewriteBase' => '/',
  'memcache.local' => '\\OC\\Memcache\\APCu',
  'apps_paths' => 
  array (
    0 => 
    array (
      'path' => '/var/www/html/apps',
      'url' => '/apps',
      'writable' => false,
    ),
    1 => 
    array (
      'path' => '/var/www/html/custom_apps',
      'url' => '/custom_apps',
      'writable' => true,
    ),
  ),
  'instanceid' => 'oc1jinwu8xxl',
  'passwordsalt' => 'LjiRwpgv9Fto1XAballgboaIjsiumj',
  'secret' => 'gZc25akvKdbYrVHLF5UqWQZkcqd2euCwHzyHxQlu7aQr1VfM',
  'trusted_domains' => 
  array (
    0 => 'nextcloud.cos.lan',
  ),
  'datadirectory' => '/var/www/html/data',
  'dbtype' => 'sqlite3',
  'version' => '23.0.0.10',
  'overwrite.cli.url' => 'http://nextcloud.cos.lan',
  'installed' => true,
  'ldapProviderFactory' => 'OCA\\User_LDAP\\LDAPProviderFactory',
);
